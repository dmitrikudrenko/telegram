package org.kudrenko.telegram.utils;

import android.widget.ImageView;

public interface ImageWorker {
    void cancel(ImageView imageView);

    void loadFromUrl(String url, ImageView imageView);

    void loadFromFile(String path, ImageView imageView);

    void loadFromUrl(String url, ImageView imageView, boolean round);

    void loadFromFile(String path, ImageView imageView, boolean round);

    void saveFromFile(String path, String targetPath);

    void saveFromUrl(String url, String targetPath);
}
