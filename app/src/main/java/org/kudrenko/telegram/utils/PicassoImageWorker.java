package org.kudrenko.telegram.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.kudrenko.telegram.components.RoundedTransformation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@EBean
public class PicassoImageWorker implements ImageWorker {
    @RootContext
    Context mContext;

    private Picasso picasso() {
        return Picasso.with(mContext);
    }

    @Override
    public void loadFromUrl(String url, ImageView imageView) {
        loadFromUrl(url, imageView, false);
    }

    @Override
    public void loadFromFile(String path, ImageView imageView) {
        loadFromFile(path, imageView, false);
    }

    @Override
    public void loadFromUrl(String url, ImageView imageView, boolean round) {
        RequestCreator requestCreator = picasso().load(url);
        if (round) {
            requestCreator.transform(new RoundedTransformation());
        }
        requestCreator.into(imageView);
    }

    @Override
    public void loadFromFile(String path, ImageView imageView, boolean round) {
        RequestCreator requestCreator = picasso().load(new File(path));
        if (round) {
            requestCreator.transform(new RoundedTransformation());
        }
        requestCreator.into(imageView);
    }

    @Override
    public void saveFromFile(String path, String targetPath) {
        picasso().load(new File(path)).into(fileTarget(targetPath));
    }

    @Override
    public void saveFromUrl(String url, String targetPath) {
        picasso().load(url).into(fileTarget(targetPath));
    }

    @Override
    public void cancel(ImageView imageView) {
        picasso().cancelRequest(imageView);
    }

    private Target fileTarget(final String targetPath) {
        return new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    File targetDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File targetFile = new File(targetDir, targetPath + ".png");
                    targetFile.createNewFile();
                    FileOutputStream outputStream = new FileOutputStream(targetFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
    }
}
