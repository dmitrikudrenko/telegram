package org.kudrenko.telegram.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;

@EBean
public class DownloadWorker {
    @RootContext
    Context mContext;

    @SystemService
    DownloadManager downloadManager;

    public void download(String url, String targetFileName) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, targetFileName);
        downloadManager.enqueue(request);
    }
}
