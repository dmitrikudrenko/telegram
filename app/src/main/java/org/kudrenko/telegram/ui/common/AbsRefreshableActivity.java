package org.kudrenko.telegram.ui.common;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.paging.listview.PagingListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.adapters.AbsPagingAdapter;
import org.kudrenko.telegram.components.BackStackPagingListView;
import org.kudrenko.telegram.components.stickylistheaders.AdapterWrapper;
import org.kudrenko.telegram.components.stickylistheaders.StickyListHeadersAdapter;
import org.kudrenko.telegram.components.stickylistheaders.StickyListHeadersListView;
import org.kudrenko.telegram.components.swiperefreshlayoutbottom.SwipeRefreshLayoutBottom;

import java.util.Arrays;
import java.util.List;

@EActivity
public abstract class AbsRefreshableActivity<Item, Holder, Adapter extends AbsPagingAdapter<Item, Holder>> extends AbsTelegramActivity
        implements SwipeRefreshLayout.OnRefreshListener, PagingListView.Pagingable, SwipeRefreshLayoutBottom.OnRefreshListener, BackStackPagingListView.Pagingable {
    public static final int LIMIT = 100;

    @ViewById(android.R.id.list)
    protected PagingListView listView;

    @ViewById(R.id.list_back)
    protected StickyListHeadersListView stickyListHeadersListView;

    @ViewById(R.id.refresh)
    protected SwipeRefreshLayout refreshLayout;

    @ViewById(R.id.refresh_bottom)
    protected SwipeRefreshLayoutBottom refreshLayoutBottom;

    @ViewById(android.R.id.empty)
    protected View emptyView;

    protected Adapter adapter;
    protected AdapterWrapper adapterWrapper;
    protected BackStackPagingListView backStackListView;

    @AfterViews
    protected void afterViews() {
        initSwipeLayout();
        initListView();

        if (!refreshOnResume())
            onRefresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (refreshOnResume())
            onRefresh();
    }

    private void initSwipeLayout() {
        if (refreshLayout != null)
            refreshLayout.setOnRefreshListener(this);
        if (refreshLayoutBottom != null)
            refreshLayoutBottom.setOnRefreshListener(this);
    }

    protected void initListView() {
        adapter = createAdapter();

        if (listView != null) {
            listView.setAdapter(adapter);

            listView.setHasMoreItems(false);
        }
        if (stickyListHeadersListView != null) {
            stickyListHeadersListView.setAreHeadersSticky(false);
            backStackListView = stickyListHeadersListView.getWrappedList();

            adapterWrapper = stickyListHeadersListView.setAdapter((StickyListHeadersAdapter) adapter);
            adapter.setOnAddMoreItemsListener(new AbsPagingAdapter.OnAddMoreItemsListener<Item>() {
                @Override
                public void onAddMoreItems(List<Item> items) {
                    adapterWrapper.notifyDataSetChanged();
                }
            });

            backStackListView.setHasMoreItems(false);
            backStackListView.setPagingableListener(this);
        }

        if (emptyView != null) {
            if (listView != null)
                listView.setEmptyView(emptyView);
            if (stickyListHeadersListView != null)
                stickyListHeadersListView.setEmptyView(emptyView);
        }
    }

    @Override
    public void onRefresh() {
        clearPagingListener();

        if (refreshLayout != null)
            refreshLayout.setRefreshing(true);
        if (refreshLayoutBottom != null)
            refreshLayoutBottom.setRefreshing(true);
        update(true);
    }

    @Override
    public void onLoadMoreItems() {
        update(false);
    }

    @UiThread
    protected void setData(Item[] items, boolean reload) {
        if (reload)
            adapter.removeAllItems();
        if (listView != null) {
            listView.onFinishLoading(items.length != 0, Arrays.asList(items));
        }
        if (backStackListView != null) {
            backStackListView.onFinishLoading(items.length != 0, Arrays.asList(items));
        }

        setPagingListener();
    }

    protected abstract Adapter createAdapter();

    protected abstract void update(boolean reload);

    @UiThread
    protected void stopRefreshing() {
        if (refreshLayout != null)
            refreshLayout.setRefreshing(false);
        if (refreshLayoutBottom != null)
            refreshLayoutBottom.setRefreshing(false);
    }

    void setPagingListener() {
        if (listView != null) {
            listView.setPagingableListener(this);
        }
        if (backStackListView != null) {
            backStackListView.setPagingableListener(this);
        }
    }

    void clearPagingListener() {
        if (listView != null) {
            listView.setPagingableListener(null);
        }
        if (backStackListView != null) {
            backStackListView.setPagingableListener(null);
        }
    }

    protected abstract boolean refreshOnResume();
}
