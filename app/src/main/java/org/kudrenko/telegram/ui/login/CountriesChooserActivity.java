package org.kudrenko.telegram.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.adapters.CountryAdapter;
import org.kudrenko.telegram.model.Country;
import org.kudrenko.telegram.ui.common.AbsTelegramActivity;

import lollipoplistview.PinnedHeaderListView;

@EActivity(R.layout.fragment_countries)
public class CountriesChooserActivity extends AbsTelegramActivity {
    @ViewById(android.R.id.list)
    PinnedHeaderListView mListView;

    @Bean
    CountryAdapter mAdapter;

    @AfterViews
    void afterViews() {
        int pinnedHeaderBackgroundColor = getResources().getColor(getResIdFromAttribute(this, android.R.attr.colorBackground));
        mAdapter.setPinnedHeaderBackgroundColor(pinnedHeaderBackgroundColor);
        mAdapter.setPinnedHeaderTextColor(getResources().getColor(R.color.pinned_header_text));
        LayoutInflater inflater = LayoutInflater.from(this);
        mListView.setPinnedHeaderView(inflater.inflate(R.layout.item_country_header, mListView, false));
        mListView.setAdapter(mAdapter);
        mListView.setOnScrollListener(mAdapter);
        mListView.setEnableHeaderTransparencyChanges(false);
        mAdapter.setCountries(countries());
    }

    @Click(R.id.menu_icon)
    void onBackClick() {
        onBackPressed();
    }

    @ItemClick(android.R.id.list)
    void onCountrySelect(Country country) {
        setResult(RESULT_OK, new Intent().putExtra("country", country));
        finish();
    }

    public static int getResIdFromAttribute(final Activity activity, final int attr) {
        if (attr == 0)
            return 0;
        final TypedValue typedValue = new TypedValue();
        activity.getTheme().resolveAttribute(attr, typedValue, true);
        return typedValue.resourceId;
    }
}
