package org.kudrenko.telegram.ui.login.fragment;

import android.view.KeyEvent;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.ViewById;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.components.FloatingEditText;

@EFragment(R.layout.fragment_login_code_input)
public class CodeInputFragment extends AbsLoginFragment {

    @ViewById(R.id.code)
    FloatingEditText codeEtx;

    @Override
    public void onConfirm() {
        String codeStr = codeEtx.getText().toString().trim();
        send(new TdApi.AuthSetCode(codeStr), resultHandler());
    }

    @Override
    protected void onError(TdApi.Error error) {
        super.onError(error);
        codeEtx.setValidateResult(false, getString(localizeError(error)));
    }

    @EditorAction(R.id.code)
    void onSubmitClick(int actionId) {
        if (actionId == KeyEvent.KEYCODE_ENDCALL) {
            onConfirm();
        }
    }
}
