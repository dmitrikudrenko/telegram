package org.kudrenko.telegram.ui.login.fragment;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.ViewById;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.model.Country;
import org.kudrenko.telegram.ui.login.CountriesChooserActivity_;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

@EFragment(R.layout.fragment_login_phone_input)
public class PhoneInputFragment extends AbsLoginFragment {
    public static final int REQUEST_CODE_COUNTRY = 200;

    @ViewById(R.id.country)
    EditText countryEtx;

    @ViewById(R.id.country_code)
    EditText countryCode;

    @ViewById(R.id.phone)
    EditText phoneEtx;

    @SystemService
    LocationManager locationManager;

    protected Country country;

    @Click(R.id.country)
    void onCountryNameClick() {
        CountriesChooserActivity_.intent(this).startForResult(REQUEST_CODE_COUNTRY);
    }

    @Override
    public void onResume() {
        super.onResume();
        phoneEtx.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phoneEtx.requestFocus();
        if (country == null) {
            try {
                String lastKnownCountry = getLastKnownCountry();
                Country country = lastKnownCountry != null ? findByName(lastKnownCountry) : null;
                setCountry(country);
            } catch (IOException e) {
                //nothing
            }
        }
    }

    private Country findByName(String country) {
        return application.helper.findByName(country);
    }

    private Country findByCode(String code) {
        return application.helper.findByCode(code);
    }

    private String getLastKnownCountry() throws IOException {
        Location location = getLastKnownLocation();
        if (location != null) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.ENGLISH);
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
        }
        return null;
    }

    private Location getLastKnownLocation() {
        Location location = null;
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        return location;
    }

    @Override
    public void onConfirm() {
        String phoneStr = phoneEtx.getText().toString().trim();
        String countryCodeStr = countryCode.getText().toString().trim();
        send(new TdApi.AuthSetPhoneNumber(countryCodeStr + phoneStr), resultHandler());
    }

    @OnActivityResult(REQUEST_CODE_COUNTRY)
    void onCountrySelect(@OnActivityResult.Extra(value = "country") Country selected) {
        setCountry(selected);
    }

    private void setCountry(Country selected) {
        country = selected;
        if (selected != null) {
            countryEtx.setText(selected.name);
            countryCode.setText(String.valueOf(selected.code));
            placeCursorAtTheEnd(countryCode);
        } else countryEtx.setText(R.string.wrong_country);
    }

    @AfterTextChange(R.id.country_code)
    void onCountryCodeChange(Editable s, TextView view) {
        if (s.length() == 0 || s.charAt(0) != '+') {
            view.setText("+" + s);
        }
        String countryCode = s.toString().replace("+", "");
        if (country == null || !countryCode.equals(String.valueOf(country.code))) {
            Country country = findByCode(countryCode);
            setCountry(country);
        }
    }

    void placeCursorAtTheEnd(EditText editText) {
        editText.setSelection(editText.getText().length());
    }

    @EditorAction(R.id.phone)
    void onSubmitClick(int actionId) {
        if (actionId == KeyEvent.KEYCODE_ENDCALL) {
            onConfirm();
        }
    }
}
