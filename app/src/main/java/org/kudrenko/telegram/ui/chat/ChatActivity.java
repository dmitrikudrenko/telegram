package org.kudrenko.telegram.ui.chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.IntArrayRes;
import org.androidannotations.annotations.res.StringRes;
import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.adapters.MessagesAdapter;
import org.kudrenko.telegram.api.Formatter;
import org.kudrenko.telegram.components.CircularContactView;
import org.kudrenko.telegram.ui.common.AbsRefreshableActivity;
import org.kudrenko.telegram.ui.photo.PhotoViewer_;

@EActivity(R.layout.activity_chat)
public class ChatActivity extends AbsRefreshableActivity<TdApi.Message, MessagesAdapter.ViewHolder, MessagesAdapter> implements View.OnClickListener {
    @Bean
    MessagesAdapter messagesAdapter;

    @Extra("chat")
    long chatId;

    @Extra("last_message")
    int lastMessage;

    @Extra("group_chat")
    boolean isGroupChat;

    @Extra("companion")
    TdApi.User companion;

    @ViewById(R.id.avatar)
    ImageView avatar;

    @ViewById(R.id.circular_avatar)
    CircularContactView circularAvatar;

    @ViewById(R.id.chat_header_text1)
    TextView chatHeaderText1;

    @ViewById(R.id.chat_header_text2)
    TextView chatHeaderText2;

    @StringRes(R.string.chat_participants_info)
    String chatParticipantsInfo;

    @Bean
    Formatter formatter;

    @ViewById(R.id.floating_action_button)
    FloatingActionButton floatingActionButton;

    @IntArrayRes(R.array.contacts_text_background_colors)
    int[] contactColors;

    @Override
    protected MessagesAdapter createAdapter() {
        ottoBus.register(messagesAdapter);
        messagesAdapter.setCompanion(companion);
        return messagesAdapter;
    }

    @Override
    protected void initListView() {
        super.initListView();
        if (floatingActionButton != null) {
            floatingActionButton.attachToListView(backStackListView);
            floatingActionButton.setOnClickListener(this);
        }
    }

    @Override
    protected void update(final boolean reload) {
        if (reload && isGroupChat) {
            send(new TdApi.GetGroupChatFull((int) chatId), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    if (object.getConstructor() == TdApi.GroupChatFull.CONSTRUCTOR) {
                        messagesAdapter.setFullGroupChatInfo((TdApi.GroupChatFull) object);
                        loadMessages(true);
                    }
                }
            });
        } else loadMessages(reload);
        if (reload) {
            reloadHeader();
        }
    }

    @Override
    protected boolean refreshOnResume() {
        return false;
    }

    private void loadMessages(final boolean reload) {
        send(new TdApi.GetChatHistory(chatId, lastMessage, getOffset(reload), LIMIT), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object instanceof TdApi.Messages) {
                    setData(((TdApi.Messages) object).messages, reload);
                }
                stopRefreshing();
            }
        });
    }

    private void reloadHeader() {
        send(new TdApi.GetChat(chatId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                    setChatInfo((TdApi.Chat) object);
                }
            }
        });
    }

    @UiThread
    public void setChatInfo(TdApi.Chat chat) {
        TdApi.ChatInfo chatInfo = chat.type;
        int contactColor = contactColors[Math.abs((int) (chat.id % contactColors.length))];
        if (chatInfo.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
            TdApi.GroupChatInfo groupChatInfo = (TdApi.GroupChatInfo) chatInfo;
            TdApi.GroupChat groupChat = groupChatInfo.groupChat;
            chatHeaderText1.setText(groupChat.title);
            chatHeaderText2.setText(String.format(chatParticipantsInfo, groupChat.participantsCount));
            show(groupChat.photoSmall, formatter.initials(groupChat), contactColor);
        } else if (chatInfo.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            TdApi.PrivateChatInfo privateChatInfo = (TdApi.PrivateChatInfo) chatInfo;
            TdApi.User user = privateChatInfo.user;
            chatHeaderText1.setText(formatter.username(user));
            chatHeaderText2.setText(formatter.format(user.status));
            show(user.photoSmall, formatter.initials(user), contactColor);
        }
    }

    private int getOffset(boolean reload) {
        return (reload ? 0 : adapter.getCount()) - 1;
    }

    @UiThread
    public void show(TdApi.File file, String initials, int color) {
        if (file.getConstructor() == TdApi.FileLocal.CONSTRUCTOR) {
            avatar.setVisibility(View.VISIBLE);
            circularAvatar.setVisibility(View.INVISIBLE);

            imageWorker().loadFromFile(((TdApi.FileLocal) file).path, avatar, true);
        } else {
            avatar.setVisibility(View.INVISIBLE);
            circularAvatar.setVisibility(View.VISIBLE);

            circularAvatar.setTextAndBackgroundColor(initials, color);
        }
    }

    @Click(R.id.menu_icon)
    void onBack() {
        finish();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.floating_action_button:
                stickyListHeadersListView.setSelection(adapter.getCount() - 1);
                break;
        }
    }

    @ItemClick(R.id.list_back)
    void onMessageClick(TdApi.Message message) {
        TdApi.File file = messagesAdapter.getFile(message);
        if (file.getConstructor() == TdApi.FileLocal.CONSTRUCTOR) {
            PhotoViewer_.intent(this).file((TdApi.FileLocal) file).start();
        }
    }
}
