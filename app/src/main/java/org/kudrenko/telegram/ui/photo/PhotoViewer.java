package org.kudrenko.telegram.ui.photo;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.ui.common.AbsTelegramActivity;

import uk.co.senab.photoview.PhotoViewAttacher;

@EActivity(R.layout.activity_photo)
public class PhotoViewer extends AbsTelegramActivity implements PopupMenu.OnMenuItemClickListener {
    @ViewById(R.id.photo_viewer)
    ImageView content;

    @ViewById(R.id.popup_menu_view)
    View popupMenuView;

    @Extra
    TdApi.FileLocal file;

    protected PhotoViewAttacher photoViewAttacher;

    @AfterViews
    void afterViews() {
        photoViewAttacher = new PhotoViewAttacher(content);
        imageWorker().loadFromFile(file.path, content);
        Toast.makeText(this, R.string.picture_was_saved, Toast.LENGTH_LONG).show();
    }

    @Click(R.id.menu_icon)
    void onBackClick() {
        flushResources();
        finish();
    }

    @Click(R.id.menu_options_icon)
    void onOptionsClick() {
        showPopupMenu(popupMenuView, R.menu.photo_menu, this);
    }

    void onSaveInGallery() {
        imageWorker().saveFromFile(file.path, String.valueOf(System.currentTimeMillis()));
    }

    void onDelete() {

    }

    @Override
    public void onBackPressed() {
        flushResources();
        super.onBackPressed();
    }

    private void flushResources() {
        if (photoViewAttacher != null) {
            photoViewAttacher.cleanup();
            photoViewAttacher = null;
        }
        if (content != null) {
            content.setImageBitmap(null);
            file = null;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.save_to_gallery:
                onSaveInGallery();
                return true;
            case R.id.delete:
                onDelete();
                return true;
            default:
                return false;
        }
    }
}
