package org.kudrenko.telegram.ui.common;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.PopupMenu;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.TelegramApplication;
import org.kudrenko.telegram.api.Errors;
import org.kudrenko.telegram.model.Country;
import org.kudrenko.telegram.ui.AbsTelegramFragment;
import org.kudrenko.telegram.ui.StandardAnimationFinishActivity;
import org.kudrenko.telegram.utils.ImageWorker;

import java.util.List;

@EActivity
public abstract class AbsTelegramActivity extends ActionBarActivity {

    @App
    protected TelegramApplication application;

    protected ImageWorker imageWorker() {
        return application.imageWorker;
    }

    @Override
    protected void onStart() {
        application.ottoBus.register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        application.ottoBus.unregister(this);
    }

    public void send(TdApi.TLFunction function, Client.ResultHandler handler) {
        application.send(function, handler);
    }

    public void send(TdApi.TLFunction function) {
        application.send(function, null);
    }

    protected void openFragment(AbsTelegramFragment fragment, boolean withSlidingAnimation) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (withSlidingAnimation) {
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right);
        }
        transaction.replace(R.id.content, fragment).addToBackStack(null).commit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void finish() {
        super.finish();
        if (!(this instanceof StandardAnimationFinishActivity))
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public List<Country> countries() {
        return application.helper.countries();
    }

    @UiThread
    protected void showError(TdApi.Error error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error).setMessage(localizeError(error));
        builder.show();
    }

    protected void onError(TdApi.Error error) {
        showError(error);
    }

    protected Client.ResultHandler resultHandler(final Client.ResultHandler handler) {
        return new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
                    onError((TdApi.Error) object);
                } else if (handler != null) {
                    handler.onResult(object);
                }
            }
        };
    }

    protected Client.ResultHandler resultHandler() {
        return resultHandler(null);
    }

    public int localizeError(TdApi.Error error) {
        return Errors.find(error.text);
    }

    protected void showPopupMenu(View v, int menu, PopupMenu.OnMenuItemClickListener listener) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.inflate(menu);
        invalidateOptionsMenu();
        popup.setOnMenuItemClickListener(listener);
        popup.show();
    }
}
