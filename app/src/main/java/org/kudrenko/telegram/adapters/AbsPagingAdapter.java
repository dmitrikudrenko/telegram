package org.kudrenko.telegram.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.paging.listview.PagingBaseAdapter;

import org.androidannotations.annotations.EBean;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.List;

@EBean
public abstract class AbsPagingAdapter<Item, Holder> extends PagingBaseAdapter<Item> {
    protected Context mContext;

    protected OnAddMoreItemsListener<Item> onAddMoreItemsListener;
    protected ListView container;

    public interface OnAddMoreItemsListener<Item> {
        void onAddMoreItems(List<Item> items);
    }

    @Override
    public void addMoreItems(List<Item> newItems) {
        super.addMoreItems(newItems);
        if (onAddMoreItemsListener != null) {
            onAddMoreItemsListener.onAddMoreItems(newItems);
        }
    }

    public AbsPagingAdapter(Context mContext, List<Item> items) {
        super(items);
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        Item item_ = getItem_(position);
        if (container == null) {
            container = (ListView) parent;
        }
        int itemViewType = getItemViewType(position);
        if (convertView == null) {
            convertView = View.inflate(mContext, layout(position, itemViewType), null);

            holder = createAndBindHolder(convertView);
        } else holder = (Holder) convertView.getTag();

        if (holder == null) {
            holder = createAndBindHolder(convertView);
        }

        bindView(holder, item_, position);

        return convertView;
    }

    private Holder createAndBindHolder(View convertView) {
        Holder holder;
        holder = createHolder();
        bindHolder(holder, convertView);
        convertView.setTag(holder);
        return holder;
    }

    protected Item getItem_(int position) {
        return (Item) getItem(position);
    }

    protected abstract void displayImage(TdApi.File file, ImageView imageView, boolean avatar);

    protected abstract void bindView(Holder holder, Item item_, int position);

    protected abstract void bindHolder(Holder holder, View convertView);

    protected abstract int layout(int position, int itemViewType);

    protected abstract Holder createHolder();

    public void setOnAddMoreItemsListener(OnAddMoreItemsListener<Item> onAddMoreItemsListener) {
        this.onAddMoreItemsListener = onAddMoreItemsListener;
    }

    protected <T extends View> T findViewById_(View view, int resource) {
        return (T) view.findViewById(resource);
    }
}
