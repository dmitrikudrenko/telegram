package org.kudrenko.telegram.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.TelegramApplication;
import org.kudrenko.telegram.api.Formatter;
import org.kudrenko.telegram.components.CircularContactView;
import org.kudrenko.telegram.otto.events.UpdateFileEvent;
import org.kudrenko.telegram.utils.ImageWorker;

import java.util.List;

@EBean
public abstract class AbsFileContainerPagingAdapter<Item, Holder> extends AbsPagingAdapter<Item, Holder> {
    @App
    TelegramApplication application;

    @Bean
    protected Formatter formatter;

    protected int densityScale;
    protected int screenHeight;

    public AbsFileContainerPagingAdapter(Context mContext, List<Item> items) {
        super(mContext, items);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        densityScale = Math.min(2, (int) Math.ceil(displayMetrics.density));
        screenHeight = displayMetrics.heightPixels;
    }

    public abstract void updateFile(TdApi.FileLocal file);

    protected ImageWorker imageWorker() {
        return application.imageWorker;
    }

    @Override
    public void displayImage(final TdApi.File file, final ImageView imageView, boolean avatar) {
        if (file.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR) {
            int id = ((TdApi.FileEmpty) file).id;
            if (id != 0) {
                application.send(new TdApi.DownloadFile(id));
            }
        } else if (file.getConstructor() == TdApi.FileLocal.CONSTRUCTOR) {
            if (avatar)
                showAvatar((TdApi.FileLocal) file, imageView);
            else show((TdApi.FileLocal) file, imageView);
        }
    }

    @UiThread
    public void showAvatar(TdApi.FileLocal file, ImageView imageView) {
        imageWorker().loadFromFile(file.path, imageView, true);
    }

    @UiThread
    public void show(TdApi.FileLocal file, ImageView imageView) {
        if (file.path.startsWith("https") || file.path.startsWith("http")) {
            imageWorker().loadFromUrl(file.path, imageView);
        } else imageWorker().loadFromFile(file.path, imageView);
    }

    @UiThread
    public void setText(TextView textView, String text) {
        textView.setText(text);
    }

    @UiThread
    public void setVisibility(View view, int visibility) {
        view.setVisibility(visibility);
    }

    @UiThread
    public void setTextAndBackgroundColor(CircularContactView view, final CharSequence text, final int backgroundColor) {
        view.setTextAndBackgroundColor(text, backgroundColor);
    }

    @Subscribe
    public void onFileUpdate(UpdateFileEvent event) {
        TdApi.FileLocal file = event.file;
        updateFile(file);
        notifyDataSetChanged();
    }
}
