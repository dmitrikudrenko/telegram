package org.kudrenko.telegram.adapters;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringRes;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.components.CircularContactView;
import org.kudrenko.telegram.otto.events.UpdateMessageEvent;
import org.kudrenko.telegram.otto.events.UpdateUserStatusEvent;

import java.util.ArrayList;

@EBean
public class ChatsAdapter extends AbsFileContainerPagingAdapter<TdApi.Chat, ChatsAdapter.ViewHolder> {
    @SystemService
    ConnectivityManager connectivityManager;

    @StringRes(R.string.chat_title_pattern)
    String chatTitlePattern;

    @StringRes(R.string.message_content_audio)
    String messageContentAudio;

    @StringRes(R.string.message_content_document)
    String messageContentDocument;

    @StringRes(R.string.message_content_sticker)
    String messageContentSticker;

    @StringRes(R.string.message_content_photo)
    String messageContentPhoto;

    @StringRes(R.string.message_content_video)
    String messageContentVideo;

    @StringRes(R.string.message_content_geopoint)
    String messageContentGeopoint;

    @StringRes(R.string.message_content_contact)
    String messageContentContact;

    @StringRes(R.string.message_content_group_chat_create)
    String messageContentGroupChatCreate;

    @StringRes(R.string.message_content_changed_title)
    String messageContentChangedTitle;

    @StringRes(R.string.message_content_changed_photo)
    String messageContentChangedPhoto;

    @StringRes(R.string.message_content_delete_photo)
    String messageContentDeletePhoto;

    @StringRes(R.string.message_content_add_participant)
    String messageContentAddParticipant;

    @StringRes(R.string.message_content_delete_participant)
    String messageContentDeleteParticipant;

    @StringRes(R.string.message_content_deleted)
    String messageContentDeleted;

    @StringRes(R.string.message_content_unsupported)
    String messageContentUnsupported;

    @ColorRes(R.color.chat_system_color)
    int chatSystemColor;

    @ColorRes(R.color.chat_simple_text_color)
    int chatSimpleTextColor;

    public static final int TYPE_COUNT = 3;
    public static final int TYPE_SIMPLE = 0;
    public static final int TYPE_BADGED = 1;
    public static final int TYPE_ERROR = 2;

    public ChatsAdapter(@SuppressWarnings("unused")Context mContext) {
        super(mContext, new ArrayList<TdApi.Chat>());
    }

    @Override
    public void updateFile(TdApi.FileLocal file) {
        for (TdApi.Chat chat : items) {
            TdApi.ChatInfo type = chat.type;
            TdApi.File chatFile;
            if (type instanceof TdApi.GroupChatInfo) {
                TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) type).groupChat;
                chatFile = groupChat.photoSmall;

                if (chatFile.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) chatFile).id == file.id) {
                    groupChat.photoSmall = file;
                    break;
                }
            } else if (type instanceof TdApi.PrivateChatInfo) {
                TdApi.User user = ((TdApi.PrivateChatInfo) type).user;
                chatFile = user.photoSmall;

                if (chatFile.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) chatFile).id == file.id) {
                    user.photoSmall = file;
                    break;
                }
            }
        }
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        TdApi.Chat item_ = getItem_(position);
        if (item_.unreadCount != 0) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return TYPE_BADGED;
            } else return TYPE_ERROR;
        }
        return TYPE_SIMPLE;
    }

    @Override
    protected void bindView(ViewHolder viewHolder, TdApi.Chat item_, int position) {
        setText(viewHolder.name, formatter.chatName(item_));
        setChatContent(viewHolder.message, item_);
        setText(viewHolder.time, formatter.convertTime(item_.topMessage.date));
        if (avatarExists(item_)) {
            setVisibility(viewHolder.avatar, View.VISIBLE);
            setVisibility(viewHolder.circularAvatar, View.INVISIBLE);

            displayImage(getFile(item_), viewHolder.avatar, true);
        } else {
            setVisibility(viewHolder.avatar, View.INVISIBLE);
            setVisibility(viewHolder.circularAvatar, View.VISIBLE);

            setTextAndBackgroundColor(viewHolder.circularAvatar, formatter.initials(item_), formatter.chatColor(item_));
        }

        if (viewHolder.badge != null) {
            setText(viewHolder.badge, String.valueOf(item_.unreadCount));
        }

        setStatusIfNeed(item_, viewHolder);
    }

    private void setStatusIfNeed(TdApi.Chat item_, ViewHolder viewHolder) {
        TdApi.ChatInfo type = item_.type;
        if (type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            TdApi.PrivateChatInfo chatInfo = (TdApi.PrivateChatInfo) type;
            TdApi.UserStatus status = chatInfo.user.status;
            if (status.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
                viewHolder.status.setVisibility(View.VISIBLE);
                viewHolder.clock.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void bindHolder(ViewHolder viewHolder, View convertView) {
        viewHolder.name = (TextView) convertView.findViewById(R.id.name);
        viewHolder.message = (TextView) convertView.findViewById(R.id.text);
        viewHolder.time = (TextView) convertView.findViewById(R.id.time);
        viewHolder.badge = (TextView) convertView.findViewById(R.id.badge_count);

        viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
        viewHolder.circularAvatar = (CircularContactView) convertView.findViewById(R.id.circular_avatar);

        viewHolder.clock = convertView.findViewById(R.id.clock);
        viewHolder.status = convertView.findViewById(R.id.status);
    }

    @Override
    protected int layout(int position, int itemViewType) {
        if (itemViewType == TYPE_BADGED) {
            return R.layout.item_chat_badge;
        } else if (itemViewType == TYPE_ERROR) {
            return R.layout.item_chat_error;
        }
        return R.layout.item_chat_simple;
    }

    protected boolean avatarExists(TdApi.Chat item_) {
        TdApi.File file = getFile(item_);
        return file.getConstructor() == TdApi.FileLocal.CONSTRUCTOR ||
                (file.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) file).id != 0);
    }

    @Override
    protected ViewHolder createHolder() {
        return new ViewHolder();
    }

    private void setChatContent(TextView textView, TdApi.Chat chat) {
        TdApi.MessageContent messageContent = chat.topMessage.message;
        if (messageContent.getConstructor() == TdApi.MessageText.CONSTRUCTOR) {
            textView.setText(((TdApi.MessageText) messageContent).text);
            textView.setTextColor(chatSimpleTextColor);
            return;
        } else if (messageContent.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
            textView.setText(messageContentAudio);
        } else if (messageContent.getConstructor() == TdApi.MessageDocument.CONSTRUCTOR) {
            textView.setText(messageContentDocument);
        } else if (messageContent.getConstructor() == TdApi.MessageSticker.CONSTRUCTOR) {
            textView.setText(messageContentSticker);
        } else if (messageContent.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
            textView.setText(messageContentPhoto);
        } else if (messageContent.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
            textView.setText(messageContentVideo);
        } else if (messageContent.getConstructor() == TdApi.MessageGeoPoint.CONSTRUCTOR) {
            textView.setText(messageContentGeopoint);
        } else if (messageContent.getConstructor() == TdApi.MessageContact.CONSTRUCTOR) {
            textView.setText(messageContentContact);
        } else if (messageContent.getConstructor() == TdApi.MessageGroupChatCreate.CONSTRUCTOR) {
            textView.setText(messageContentGroupChatCreate);
        } else if (messageContent.getConstructor() == TdApi.MessageChatChangeTitle.CONSTRUCTOR) {
            textView.setText(messageContentChangedTitle);
        } else if (messageContent.getConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR) {
            textView.setText(messageContentChangedPhoto);
        } else if (messageContent.getConstructor() == TdApi.MessageChatDeletePhoto.CONSTRUCTOR) {
            textView.setText(messageContentDeletePhoto);
        } else if (messageContent.getConstructor() == TdApi.MessageChatAddParticipant.CONSTRUCTOR) {
            textView.setText(String.format(messageContentAddParticipant, formatter.username(((TdApi.MessageChatAddParticipant) messageContent).user)));
        } else if (messageContent.getConstructor() == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR) {
            textView.setText(String.format(messageContentDeleteParticipant, formatter.username(((TdApi.MessageChatDeleteParticipant) messageContent).user)));
        } else if (messageContent.getConstructor() == TdApi.MessageDeleted.CONSTRUCTOR) {
            textView.setText(messageContentDeleted);
        } else if (messageContent.getConstructor() == TdApi.MessageUnsupported.CONSTRUCTOR) {
            textView.setText(messageContentUnsupported);
        }
        textView.setTextColor(chatSystemColor);
    }

    protected TdApi.File getFile(TdApi.Chat chat) {
        TdApi.ChatInfo type = chat.type;
        if (type instanceof TdApi.GroupChatInfo) {
            TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) type).groupChat;
            return groupChat.photoSmall;
        } else if (type instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) type).user;
            return user.photoSmall;
        }
        return null;
    }

    public static class ViewHolder {
        TextView name;
        TextView message;
        TextView time;
        TextView badge;

        ImageView avatar;
        CircularContactView circularAvatar;

        View status;
        View clock;
    }

    @Subscribe
    public void onMessageUpdate(UpdateMessageEvent event) {
        TdApi.Message message = event.message;
        for (TdApi.Chat chat : items) {
            if (chat.id == message.chatId) {
                chat.topMessage = message;
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Subscribe
    public void onUserStatusUpdate(UpdateUserStatusEvent event) {
        int userId = event.userId;
        TdApi.UserStatus userStatus = event.userStatus;
        for (TdApi.Chat chat : items) {
            if (chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                TdApi.PrivateChatInfo chatInfo = (TdApi.PrivateChatInfo) chat.type;
                if (chatInfo.user.id == userId) {
                    if (chatInfo.user.status.getConstructor() != userStatus.getConstructor()) {
                        chatInfo.user.status = userStatus;
                        notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
    }
}
