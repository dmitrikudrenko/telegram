package org.kudrenko.telegram.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.components.CircularContactView;
import org.kudrenko.telegram.components.stickylistheaders.StickyListHeadersAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@EBean
public class MessagesAdapter extends AbsFileContainerPagingAdapter<TdApi.Message, MessagesAdapter.ViewHolder>
        implements StickyListHeadersAdapter {

    public static final String GOOGLE_MAP_PREVIEW_URL = "https://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=13&size=400x200&maptype=roadmap&scale=%d&markers=color:red|size:big|%f,%f&sensor=false";

    @Bean
    MessageTypes messageTypes;

    protected TdApi.GroupChatFull fullGroupChatInfo;
    protected TdApi.User companion;

    public MessagesAdapter(Context mContext) {
        super(mContext, new ArrayList<TdApi.Message>());
    }

    @AfterInject
    void afterInject() {
        messageTypes.setFullGroupChatInfo(fullGroupChatInfo);
    }

    @Override
    public void addMoreItems(List<TdApi.Message> newItems) {
        for (TdApi.Message item : newItems) {
            items.add(0, item);
        }
        if (onAddMoreItemsListener != null) {
            onAddMoreItemsListener.onAddMoreItems(newItems);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return messageTypes.getTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        TdApi.Message item_ = getItem_(position);
        return messageTypes.getType(item_);
    }

    @Override
    public void updateFile(TdApi.FileLocal file) {
        int id = file.id;
        for (TdApi.Message message : items) {
            TdApi.MessageContent content = message.message;
            if (content.getConstructor() == TdApi.MessageSticker.CONSTRUCTOR) {
                TdApi.File sticker = ((TdApi.MessageSticker) content).sticker.sticker;
                if (sticker.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) sticker).id == id) {
                    ((TdApi.MessageSticker) content).sticker.sticker = file;
                    return;
                }
            } else if (content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
                TdApi.File photo = ((TdApi.MessageVideo) content).video.thumb.photo;
                if (photo.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) photo).id == id) {
                    ((TdApi.MessageVideo) content).video.thumb.photo = file;
                    return;
                }
            } else if (content.getConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR) {
                TdApi.Photo photo = ((TdApi.MessageChatChangePhoto) content).photo;
                if (photo.photos != null && photo.photos.length > 0) {
                    for (TdApi.PhotoSize size : photo.photos) {
                        TdApi.File photoFile = size.photo;
                        if (photoFile.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) photoFile).id == id) {
                            size.photo = file;
                            return;
                        }
                    }
                }
            } else if (content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
                TdApi.Photo photo = ((TdApi.MessagePhoto) content).photo;
                if (photo.photos != null && photo.photos.length > 0) {
                    for (TdApi.PhotoSize size : photo.photos) {
                        TdApi.File photoFile = size.photo;
                        if (photoFile.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) photoFile).id == id) {
                            size.photo = file;
                            return;
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void bindView(ViewHolder viewHolder, TdApi.Message item_, int position) {
        if (viewHolder.text != null)
            viewHolder.text.setText(getMessageText(item_));
        if (viewHolder.time != null)
            viewHolder.time.setText(formatter.convertTime(item_.date));

        if (viewHolder.inviting != null && viewHolder.participant != null) {
            TdApi.User participant = getParticipant(item_);
            viewHolder.participant.setText(formatter.username(participant));
            viewHolder.inviting.setText(formatter.username(getInviting(participant)));
        }
        if (viewHolder.actionUser != null) {
            if (viewHolder.title != null) {
                TdApi.MessageChatChangeTitle changeTitle = (TdApi.MessageChatChangeTitle) item_.message;
                viewHolder.title.setText(changeTitle.title);
            } else if (viewHolder.groupName != null) {
                TdApi.MessageGroupChatCreate chatCreate = (TdApi.MessageGroupChatCreate) item_.message;
                viewHolder.groupName.setText(chatCreate.title);
            } else if (viewHolder.leaver != null) {
                TdApi.MessageChatDeleteParticipant deleteParticipant = (TdApi.MessageChatDeleteParticipant) item_.message;
                viewHolder.leaver.setText(formatter.username(deleteParticipant.user));
            }
            viewHolder.actionUser.setText(formatter.username(getUser(item_)));
        }
        if (viewHolder.audioTime != null) {
            viewHolder.audioTime.setText(getAudioTime(item_));
        }
        if (viewHolder.contentImage != null) {
            //todo solve blink problem
//            viewHolder.contentImage.setImageBitmap(null);

            SizedFile sizedFile = getMessageContentFile(item_);
            if (sizedFile != null) {
                setSizeToContentImage(viewHolder.contentImage, sizedFile);
                displayImage(sizedFile.file, viewHolder.contentImage, false);
            }
        }
        if (viewHolder.contactAvatar != null) {
            TdApi.MessageContact contact = (TdApi.MessageContact) item_.message;
            viewHolder.contactName.setText(formatter.username(contact.firstName, contact.lastName));
            viewHolder.contactPhone.setText(contact.phoneNumber);
        }
        if (viewHolder.documentName != null) {
            TdApi.MessageDocument document = (TdApi.MessageDocument) item_.message;
            viewHolder.documentName.setText(document.document.fileName);
            TdApi.File file = document.document.document;
            viewHolder.documentSize.setText(formatter.formatSize(file.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR ?
                    ((TdApi.FileEmpty) file).size : ((TdApi.FileLocal) file).size));
        }
        bindUser(viewHolder, item_);
    }

    private void setSizeToContentImage(ImageView contentImage, SizedFile sizedFile) {
        ViewGroup.LayoutParams layoutParams = contentImage.getLayoutParams();
        layoutParams.width = sizedFile.width;
        layoutParams.height = sizedFile.height;
    }

    private SizedFile getMessageContentFile(TdApi.Message item_) {
        TdApi.MessageContent content = item_.message;
        if (content.getConstructor() == TdApi.MessageSticker.CONSTRUCTOR) {
            TdApi.MessageSticker sticker = (TdApi.MessageSticker) content;
            return new SizedFile(sticker.sticker.sticker, sticker.sticker.width, sticker.sticker.height);
        } else if (content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
            TdApi.Photo photo = ((TdApi.MessagePhoto) content).photo;
            TdApi.PhotoSize photoSize = selectAppropriateSize(photo.photos);
            if (photoSize != null) {
                return new SizedFile(photoSize.photo, photoSize.width, photoSize.height);
            }
        } else if (content.getConstructor() == TdApi.MessageGeoPoint.CONSTRUCTOR) {
            TdApi.MessageGeoPoint geoPoint = (TdApi.MessageGeoPoint) content;
            String url = String.format(Locale.US, GOOGLE_MAP_PREVIEW_URL,
                    geoPoint.geoPoint.latitude, geoPoint.geoPoint.longitude, densityScale,
                    geoPoint.geoPoint.latitude, geoPoint.geoPoint.longitude);
            return new SizedFile(new TdApi.FileLocal(0, 0, url), 800, 400);
        } else if (content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
            TdApi.MessageVideo video = (TdApi.MessageVideo) content;
            return new SizedFile(video.video.thumb.photo, video.video.width, video.video.height);
        } else if (content.getConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR) {
            TdApi.Photo photo = ((TdApi.MessageChatChangePhoto) content).photo;
            TdApi.PhotoSize photoSize = selectAppropriateSize(photo.photos);
            if (photoSize != null) {
                return new SizedFile(photoSize.photo, photoSize.width, photoSize.height);
            }
        }
        return null;
    }

    private TdApi.PhotoSize selectAppropriateSize(TdApi.PhotoSize[] sizes) {
        if (sizes == null || sizes.length == 0) return null;

        for (TdApi.PhotoSize size : sizes) {
            int height = size.height;
            if (height >= screenHeight) {
                return size;
            }
        }
        return sizes[sizes.length - 1];
    }

    private String getAudioTime(TdApi.Message item_) {
        TdApi.MessageContent content = item_.message;
        if (content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
            TdApi.MessageAudio audio = (TdApi.MessageAudio) content;
            return formatter.formatDuration(audio.audio.duration);
        }
        return null;
    }

    @Override
    protected void bindHolder(ViewHolder viewHolder, View convertView) {
        viewHolder.avatar = findViewById_(convertView, R.id.avatar);
        viewHolder.circularAvatar = findViewById_(convertView, R.id.circular_avatar);
        viewHolder.username = findViewById_(convertView, R.id.username);
        viewHolder.time = findViewById_(convertView, R.id.time);
        viewHolder.text = findViewById_(convertView, R.id.text);

        viewHolder.inviting = findViewById_(convertView, android.R.id.text1);
        viewHolder.participant = findViewById_(convertView, android.R.id.text2);

        viewHolder.actionUser = findViewById_(convertView, R.id.action_user);
        viewHolder.title = findViewById_(convertView, R.id.change_title);
        viewHolder.groupName = findViewById_(convertView, R.id.group_name);
        viewHolder.leaver = findViewById_(convertView, R.id.leaver);

        viewHolder.contentImage = findViewById_(convertView, R.id.message_content);

        viewHolder.audioTime = findViewById_(convertView, R.id.audio_time);

        viewHolder.contactName = findViewById_(convertView, R.id.contact_name);
        viewHolder.contactPhone = findViewById_(convertView, R.id.contact_phone);
        viewHolder.contactAvatar = findViewById_(convertView, R.id.contact_avatar);

        viewHolder.documentName = findViewById_(convertView, R.id.document_name);
        viewHolder.documentSize = findViewById_(convertView, R.id.document_size);
    }

    @Override
    protected int layout(int position, int itemViewType) {
        return messageTypes.getLayout(itemViewType);
    }

    @Override
    protected ViewHolder createHolder() {
        return new ViewHolder();
    }

    private CharSequence getMessageText(TdApi.Message item_) {
        TdApi.MessageContent content = item_.message;
        if (content.getConstructor() == TdApi.MessageText.CONSTRUCTOR) {
            return ((TdApi.MessageText) content).text;
        }
        return null;
    }

    private TdApi.User getParticipant(TdApi.Message message) {
        TdApi.MessageContent content = message.message;
        if (content.getConstructor() == TdApi.MessageChatAddParticipant.CONSTRUCTOR) {
            return ((TdApi.MessageChatAddParticipant) content).user;
        } else if (content.getConstructor() == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR) {
            return ((TdApi.MessageChatDeleteParticipant) content).user;
        }
        return null;
    }

    private TdApi.User getInviting(TdApi.User participant) {
        int invitingId = 0;
        for (TdApi.ChatParticipant chatParticipant : fullGroupChatInfo.participants) {
            if (chatParticipant.user.id == participant.id) {
                invitingId = chatParticipant.inviterId;
                break;
            }
        }
        for (TdApi.ChatParticipant chatParticipant : fullGroupChatInfo.participants) {
            if (chatParticipant.user.id == invitingId) {
                return chatParticipant.user;
            }
        }
        return null;
    }

    protected void bindUser(final ViewHolder holder, TdApi.Message message) {
        if (holder.username != null) {
            setText(holder.username, getUsername(message));
        }
        if (holder.avatar != null) {
            setAvatar(holder, getUser(message));
        }
    }

    private String getUsername(TdApi.Message message) {
        TdApi.User user = getUser(message);
        return user != null ? formatter.username(user) : null;
    }

    private TdApi.User getUser(TdApi.Message message) {
        int fromId = message.fromId;
        if (companion != null) {
            if (companion.id == fromId)
                return companion;
            else if (application.me.id == fromId)
                return application.me;
        } else if (fullGroupChatInfo != null) {
            for (TdApi.ChatParticipant participant : fullGroupChatInfo.participants) {
                TdApi.User user = participant.user;
                if (user.id == fromId) {
                    return user;
                }
            }
        }
        return null;
    }

    public void setAvatar(ViewHolder holder, TdApi.User user) {
        if (user == null) return;

        TdApi.File file = user.photoSmall;
        if (file.getConstructor() == TdApi.FileEmpty.CONSTRUCTOR && ((TdApi.FileEmpty) file).id == 0) {
            setVisibility(holder.avatar, View.INVISIBLE);
            setVisibility(holder.circularAvatar, View.VISIBLE);

            setTextAndBackgroundColor(holder.circularAvatar, formatter.initials(user), formatter.userColor(user));
        } else {
            displayImage(file, holder.avatar, true);

            setVisibility(holder.avatar, View.VISIBLE);
            setVisibility(holder.circularAvatar, View.INVISIBLE);
        }
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = View.inflate(mContext, R.layout.item_chat_separator, null);
            holder.text = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        TdApi.Message item_ = getItem_(position);
        String headerText = formatter.convertDate(item_.date);
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        TdApi.Message item_ = getItem_(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(item_.date * 1000L);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    public TdApi.File getFile(TdApi.Message message) {
        SizedFile sizedFile = getMessageContentFile(message);
        return sizedFile != null ? sizedFile.file : null;
    }

    public class ViewHolder {
        TextView username;
        TextView time;
        TextView text;

        TextView inviting;
        TextView participant;

        TextView actionUser;
        TextView title;
        TextView groupName;
        TextView leaver;

        TextView audioTime;

        ImageView contentImage;

        TextView contactName;
        TextView contactPhone;
        ImageView contactAvatar;

        TextView documentName;
        TextView documentSize;

        ImageView avatar;
        CircularContactView circularAvatar;
    }

    public class HeaderViewHolder {
        TextView text;
    }

    public void setFullGroupChatInfo(TdApi.GroupChatFull fullGroupChatInfo) {
        this.fullGroupChatInfo = fullGroupChatInfo;
    }

    public void setCompanion(TdApi.User companion) {
        this.companion = companion;
    }

    public class SizedFile {
        TdApi.File file;
        int width;
        int height;

        public SizedFile(TdApi.File file, int width, int height) {
            this.file = file;
            this.width = width;
            this.height = height;
        }
    }
}
