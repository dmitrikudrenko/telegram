package org.kudrenko.telegram.adapters;

import org.androidannotations.annotations.EBean;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;

@EBean
public class MessageTypes {
    private static final int TYPE_COUNT = 16;

    private static final int TYPE_SIMPLE = 0;
    private static final int TYPE_ADD_PARTICIPANT = 1;
    private static final int TYPE_AUDIO = 2;
    private static final int TYPE_STICKER = 3;
    private static final int TYPE_GEOPOINT = 4;
    private static final int TYPE_TITLE_CHANGE = 5;
    private static final int TYPE_UNSUPPORTED = 6;
    private static final int TYPE_VIDEO = 7;
    private static final int TYPE_GROUP_CHAT_CREATING = 8;
    private static final int TYPE_GROUP_CHAT_PHOTO_CHANGED = 9;
    private static final int TYPE_DELETE_PARTICIPANT = 10;
    private static final int TYPE_PHOTO = 11;
    private static final int TYPE_CHAT_PHOTO_DELETE = 12;
    private static final int TYPE_CONTACT = 13;
    private static final int TYPE_DOCUMENT = 14;
    private static final int TYPE_KICKING = 15;

    private TdApi.GroupChatFull fullGroupChatInfo;

    public int getTypeCount() {
        return TYPE_COUNT;
    }

    public int getType(TdApi.Message message) {
        TdApi.MessageContent content = message.message;
        if (content.getConstructor() == TdApi.MessageText.CONSTRUCTOR) {
            return TYPE_SIMPLE;
        } else if (content.getConstructor() == TdApi.MessageChatAddParticipant.CONSTRUCTOR) {
            return TYPE_ADD_PARTICIPANT;
        } else if (content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
            return TYPE_AUDIO;
        } else if (content.getConstructor() == TdApi.MessageSticker.CONSTRUCTOR) {
            return TYPE_STICKER;
        } else if (content.getConstructor() == TdApi.MessageGeoPoint.CONSTRUCTOR) {
            return TYPE_GEOPOINT;
        } else if (content.getConstructor() == TdApi.MessageChatChangeTitle.CONSTRUCTOR) {
            return TYPE_TITLE_CHANGE;
        } else if (content.getConstructor() == TdApi.MessageUnsupported.CONSTRUCTOR) {
            return TYPE_UNSUPPORTED;
        } else if (content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
            return TYPE_VIDEO;
        } else if (content.getConstructor() == TdApi.MessageGroupChatCreate.CONSTRUCTOR) {
            return TYPE_GROUP_CHAT_CREATING;
        } else if (content.getConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR) {
            return TYPE_GROUP_CHAT_PHOTO_CHANGED;
        } else if (content.getConstructor() == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR) {
            if (isKicking(message))
                return TYPE_KICKING;
            return TYPE_DELETE_PARTICIPANT;
        } else if (content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
            return TYPE_PHOTO;
        } else if (content.getConstructor() == TdApi.MessageChatDeletePhoto.CONSTRUCTOR) {
            return TYPE_CHAT_PHOTO_DELETE;
        } else if (content.getConstructor() == TdApi.MessageContact.CONSTRUCTOR) {
            return TYPE_CONTACT;
        } else if (content.getConstructor() == TdApi.MessageDocument.CONSTRUCTOR) {
            return TYPE_DOCUMENT;
        }
        return TYPE_UNSUPPORTED;
    }

    public int getLayout(int itemViewType) {
        if (itemViewType == TYPE_SIMPLE) {
            return R.layout.item_message_simple;
        } else if (itemViewType == TYPE_ADD_PARTICIPANT) {
            return R.layout.item_message_add_participant;
        } else if (itemViewType == TYPE_AUDIO) {
            return R.layout.item_message_audio;
        } else if (itemViewType == TYPE_STICKER) {
            return R.layout.item_message_image;
        } else if (itemViewType == TYPE_GEOPOINT) {
            return R.layout.item_message_image;
        } else if (itemViewType == TYPE_TITLE_CHANGE) {
            return R.layout.item_message_title_changed;
        } else if (itemViewType == TYPE_UNSUPPORTED) {
            return R.layout.item_message_unsupported;
        } else if (itemViewType == TYPE_VIDEO) {
            return R.layout.item_message_video;
        } else if (itemViewType == TYPE_GROUP_CHAT_CREATING) {
            return R.layout.item_message_group_created;
        } else if (itemViewType == TYPE_GROUP_CHAT_PHOTO_CHANGED) {
            return R.layout.item_message_chat_photo_changed;
        } else if (itemViewType == TYPE_DELETE_PARTICIPANT) {
            return R.layout.item_message_leaving;
        } else if (itemViewType == TYPE_PHOTO) {
            return R.layout.item_message_image;
        } else if (itemViewType == TYPE_CHAT_PHOTO_DELETE) {
            return R.layout.item_message_delete_chat_photo;
        } else if (itemViewType == TYPE_CONTACT) {
            return R.layout.item_message_contact;
        } else if (itemViewType == TYPE_DOCUMENT) {
            return R.layout.item_message_document;
        } else if (itemViewType == TYPE_KICKING) {
            return R.layout.item_message_kicking;
        }
        return R.layout.item_message_unsupported;
    }

    public void setFullGroupChatInfo(TdApi.GroupChatFull fullGroupChatInfo) {
        this.fullGroupChatInfo = fullGroupChatInfo;
    }

    private boolean isKicking(TdApi.Message message) {
        return isAdmin(message.fromId);
    }

    private boolean isAdmin(int fromId) {
        return fullGroupChatInfo.adminId == fromId;
    }
}
