package org.kudrenko.telegram.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.kudrenko.telegram.R;
import org.kudrenko.telegram.model.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lollipoplistview.SearchablePinnedHeaderListViewAdapter;
import lollipoplistview.StringArrayAlphabetIndexer;

@EBean
public class CountryAdapter extends SearchablePinnedHeaderListViewAdapter<Country> {
    @RootContext
    Context mContext;

    protected ArrayList<Country> countries = new ArrayList<>();

    @Override
    public boolean doFilter(Country item, CharSequence constraint) {
        if (TextUtils.isEmpty(constraint))
            return true;
        final String displayName = item.name;
        return !TextUtils.isEmpty(displayName) && displayName.toLowerCase(Locale.getDefault())
                .contains(constraint.toString().toLowerCase(Locale.getDefault()));
    }

    @Override
    public ArrayList<Country> getOriginalList() {
        return countries;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final View rootView;
        if (convertView == null) {
            holder = new ViewHolder();
            rootView = View.inflate(mContext, R.layout.item_country, null);

            holder.name = (TextView) rootView.findViewById(android.R.id.text1);
            holder.code = (TextView) rootView.findViewById(android.R.id.text2);
            holder.headerView = (TextView) rootView.findViewById(R.id.header_text);

            rootView.setTag(holder);
        } else {
            rootView = convertView;
            holder = (ViewHolder) rootView.getTag();
        }

        final Country item = getItem(position);
        holder.name.setText(item.name);
        holder.code.setText("+" + item.code);

        bindSectionHeader(holder.headerView, null, position);
        return rootView;
    }

    public void setCountries(List<Country> countries) {
        this.countries.addAll(countries);
        final String[] generatedContactNames = generateContactNames(this.countries);
        setSectionIndexer(new StringArrayAlphabetIndexer(generatedContactNames, true));
        notifyDataSetChanged();
    }

    private String[] generateContactNames(final List<Country> countries) {
        final ArrayList<String> contactNames = new ArrayList<>();
        if (countries != null)
            for (final Country contactEntity : countries)
                contactNames.add(contactEntity.name);
        return contactNames.toArray(new String[contactNames.size()]);
    }

    class ViewHolder {
        TextView name, code;
        TextView headerView;
    }
}
