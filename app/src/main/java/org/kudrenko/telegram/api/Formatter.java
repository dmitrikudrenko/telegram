package org.kudrenko.telegram.api;

import android.content.Context;
import android.text.TextUtils;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.IntArrayRes;
import org.androidannotations.annotations.res.StringRes;
import org.drinkless.td.libcore.telegram.TdApi;
import org.kudrenko.telegram.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@EBean
public class Formatter {
    public static final int MILLS_PER_SECOND = 1000;
    public static final int SEC_IN_MINUTE = 60;
    public static final int MINUTE_IN_HOUR = 60;
    public static final int SEC_IN_HOUR = SEC_IN_MINUTE * MINUTE_IN_HOUR;
    public static final int HOUR_IN_DAY = 24;

    @StringRes(R.string.user_login)
    String userLoginPattern;

    @StringRes(R.string.user_status_online)
    String userStatusOnline;

    @StringRes(R.string.user_status_offline)
    String userStatusOffline;

    @StringRes(R.string.user_status_recently)
    String userStatusRecently;

    @StringRes(R.string.user_status_last_week)
    String userStatusLastWeek;

    @StringRes(R.string.user_status_last_month)
    String userStatusLastMonth;

    @StringRes(R.string.yesterday)
    String yesterdayStr;

    @IntArrayRes(R.array.contacts_text_background_colors)
    int[] contactColors;

    public Formatter(@SuppressWarnings("unused") Context mContext) {
    }

    public String username(String firstName, String lastName) {
        return String.format(userLoginPattern, firstName, lastName);
    }

    public String username(TdApi.User user) {
        if (!TextUtils.isEmpty(user.username)) {
            return user.username;
        }
        return username(user.firstName, user.lastName);
    }

    public String initials(TdApi.User user) {
        if (!TextUtils.isEmpty(user.username)) {
            return user.username.substring(0, 1);
        }
        return user.firstName.substring(0, 1) + (!TextUtils.isEmpty(user.lastName) ? user.lastName.substring(0, 1) : "");
    }

    public String initials(TdApi.GroupChat groupChat) {
        return groupChat.title.substring(0, 1);
    }

    public String initials(TdApi.Chat chat) {
        TdApi.ChatInfo type = chat.type;
        if (type instanceof TdApi.GroupChatInfo) {
            TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) type).groupChat;
            return initials(groupChat);
        } else if (type instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) type).user;
            return initials(user);
        }
        return null;
    }

    public String convertTime(int time) {
        Date date = new Date(time * 1000L);
        return SimpleDateFormat.getTimeInstance(DateFormat.SHORT, Locale.ENGLISH).format(date);
    }

    public String convertDate(int time) {
        Date date = new Date(time * 1000L);
        return new SimpleDateFormat("MMMM dd", Locale.US).format(date);
    }

    public String chatName(TdApi.Chat chat) {
        TdApi.ChatInfo type = chat.type;
        if (type instanceof TdApi.GroupChatInfo) {
            TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) type).groupChat;
            return groupChat.title;
        } else if (type instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) type).user;
            return username(user);
        }
        return "";
    }

    public String format(TdApi.UserStatus status) {
        if (status.getConstructor() == TdApi.UserStatusEmpty.CONSTRUCTOR) {
            return "";
        } else if (status.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
            return userStatusOnline;
        } else if (status.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR) {
            TdApi.UserStatusOffline statusOffline = (TdApi.UserStatusOffline) status;

            Calendar current = Calendar.getInstance();
            Calendar wasOnline = Calendar.getInstance();
            wasOnline.setTimeInMillis(statusOffline.wasOnline * 1000L);
            String day = current.get(Calendar.DAY_OF_YEAR) == wasOnline.get(Calendar.DAY_OF_YEAR) ? "" :
                    " " + (current.get(Calendar.DAY_OF_YEAR) - 1 == wasOnline.get(Calendar.DAY_OF_YEAR) ? yesterdayStr :
                            convertDate(statusOffline.wasOnline));
            String time = convertTime(statusOffline.wasOnline);

            return String.format(userStatusOffline, day, time);
        } else if (status.getConstructor() == TdApi.UserStatusRecently.CONSTRUCTOR) {
            return userStatusRecently;
        } else if (status.getConstructor() == TdApi.UserStatusLastWeek.CONSTRUCTOR) {
            return userStatusLastWeek;
        } else if (status.getConstructor() == TdApi.UserStatusLastMonth.CONSTRUCTOR) {
            return userStatusLastMonth;
        }
        return "";
    }

    public String formatDuration(int duration) {
        DecimalFormat format = new DecimalFormat("00");

        int second = duration % SEC_IN_MINUTE;
        int minute = (duration / SEC_IN_MINUTE) % MINUTE_IN_HOUR;
        int hour = (duration / SEC_IN_HOUR) % HOUR_IN_DAY;

        return String.format("%s:%s", format.format(minute + hour * MINUTE_IN_HOUR), format.format(second));
    }

    public int chatColor(TdApi.Chat chat) {
        return contactColors[Math.abs((int) (chat.id % contactColors.length))];
    }

    public int userColor(TdApi.User user) {
        return contactColors[Math.abs(user.id % contactColors.length)];
    }

    public String formatSize(int i) {
        return String.valueOf(i) + "b.";
    }
}
