package org.kudrenko.telegram.otto.events;

import org.drinkless.td.libcore.telegram.TdApi;

public class UpdateUserStatusEvent extends AbsEvent {
    public int userId;
    public TdApi.UserStatus userStatus;

    public UpdateUserStatusEvent(int userId, TdApi.UserStatus userStatus) {
        this.userId = userId;
        this.userStatus = userStatus;
    }
}
