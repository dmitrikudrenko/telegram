package org.kudrenko.telegram.otto.events;

import org.drinkless.td.libcore.telegram.TdApi;

public class UpdateMessageEvent extends AbsEvent {
    public TdApi.Message message;

    public UpdateMessageEvent(TdApi.Message message) {
        this.message = message;
    }
}
