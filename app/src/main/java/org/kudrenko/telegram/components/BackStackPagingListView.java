package org.kudrenko.telegram.components;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.paging.listview.LoadingView;
import com.paging.listview.PagingBaseAdapter;

import org.kudrenko.telegram.R;

import java.util.List;

public class BackStackPagingListView extends ListView {

    public interface Pagingable {
        void onLoadMoreItems();
    }

    private boolean isLoading;
    private boolean hasMoreItems;
    private Pagingable pagingableListener;
    private LoadingView loadingView;
    private int scrollState;

    private OnScrollListener onScrollListener;

    public BackStackPagingListView(Context context) {
        super(context);
        init();
    }

    public BackStackPagingListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BackStackPagingListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setLoadingText(@StringRes int stringRes) {
        ((TextView) loadingView.findViewById(R.id.video_item_label)).setText(stringRes);
    }

    public void setLoadingText(String text) {
        ((TextView) loadingView.findViewById(R.id.video_item_label)).setText(text);
    }

    public void setPagingableListener(Pagingable pagingableListener) {
        this.pagingableListener = pagingableListener;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
        if (!hasMoreItems) {
            removeHeaderView(loadingView);
        } else if (findViewById(R.id.loading_view) == null) {
            addHeaderView(loadingView);
            ListAdapter adapter = ((HeaderViewListAdapter) getAdapter()).getWrappedAdapter();
            setAdapter(adapter);
        }
    }

    public boolean hasMoreItems() {
        return hasMoreItems;
    }

    public void onFinishLoading(boolean hasMoreItems, List newItems) {
        setHasMoreItems(hasMoreItems);
        setIsLoading(false);
        if (newItems != null && newItems.size() > 0) {
            ListAdapter adapter = ((HeaderViewListAdapter) getAdapter()).getWrappedAdapter();
            if (adapter instanceof PagingBaseAdapter) {
                ((PagingBaseAdapter) adapter).addMoreItems(newItems);
            } else if (adapter instanceof AdapterContainer) {
                ListAdapter delegate = ((AdapterContainer) adapter).getAdapter();
                if (delegate instanceof PagingBaseAdapter) {
                    ((PagingBaseAdapter) delegate).addMoreItems(newItems);
                }
            }
            //todo ugly fix
            setSelection(newItems.size() - 1);
        }
    }


    private void init() {
        isLoading = false;
        loadingView = new LoadingView(getContext());
        addHeaderView(loadingView);
        super.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                setScrollState(scrollState);
                //Dispatch to child OnScrollListener
                if (onScrollListener != null) {
                    onScrollListener.onScrollStateChanged(view, scrollState);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //Dispatch to child OnScrollListener
                if (onScrollListener != null) {
                    onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                }

                if (!isLoading && hasMoreItems && (firstVisibleItem == 0)) {
                    if (pagingableListener != null) {
                        isLoading = true;
                        pagingableListener.onLoadMoreItems();
                    }

                }
            }
        });
    }

    private void setScrollState(int scrollState) {
        this.scrollState = scrollState;
    }

    private boolean isScrolling() {
        return scrollState != OnScrollListener.SCROLL_STATE_IDLE;
    }

    @Override
    public void setOnScrollListener(OnScrollListener listener) {
        onScrollListener = listener;
    }
}
