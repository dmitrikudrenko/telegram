package org.kudrenko.telegram.components;

import android.widget.ListAdapter;

public interface AdapterContainer {
    ListAdapter getAdapter();
}
